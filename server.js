var express = require('express');
var app     = express();
var routes  = require('./src/routes');
var config  = require('./src/config/config');

// For testing if the thing works correctly
app.get('/', (req, res) => {
    console.log('Got a GET request!');
});

app.use('/api/v1/', routes);

var port = config.port;

app.listen(port, () => { 
    console.log("Listening on port " +port+ "!");
});
