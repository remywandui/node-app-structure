var express = require('express');
var app     = express.Router();
var birds   = require('./birds');

// Test that the routes pack is fine
app.get('/', function(req, res) {
    res.send("Got a GET request to the home URL");
});

app.use('/birds', birds);

module.exports = app;